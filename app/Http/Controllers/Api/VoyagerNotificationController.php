<?php

namespace App\Http\Controllers\Api;

use App\Identificator;
use App\Notification;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;


class VoyagerNotificationController extends VoyagerBaseController
{
    //
    private $private_key = 'AAAARA75XYs:APA91bEZhG9jA8RyMRcfmhFR5K1RP6iLlBqbu2WwXaQHHGBIS6hEXBuhzHSUy4AUpzfuikQitnRvwcgEeVXNjHR3GlFprFVLwC0Yc_MNjCTMjrMvT0THPP39CVYujCdNPysEyEji2P7A';

    public function store(Request $request)
    {


            $notification = Notification::create([
                'identificator_id' => $request->identificator_id,
                'title' => $request->title
            ]);
            $identificator = Identificator::find($notification->identificator_id);
            $data = [
                    "to" => $identificator->device_id,
                    "notification" =>
                        [
                            "title" => 'Уведомление',
                            "body" => $request->title,
                            "icon" => asset('/images/logo.svg'),
                            "badge" => 1

                        ],
            ];
            $dataString = json_encode($data);

            $headers = [
              'Authorization: key=' . $this->private_key,
              'Content-Type: application/json',
            ];

                $ch = curl_init();

                curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);

                curl_exec($ch);

                return back()->with('message','Успешно');
            }





}
