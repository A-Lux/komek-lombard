<?php

namespace App\Http\Controllers\Api;

use App\Contact;
use App\Http\Controllers\Controller;

class ContactsController extends Controller
{
    public function index($id)
    {
        $contact = Contact::find($id);
        $contact['address'] = explode(';', $contact['address']);
        $contact['phone'] = explode(';', $contact['phone']);
        $contact['work_time'] = explode(';', $contact['work_time']);
        $contact['break_time'] = explode(';', $contact['break_time']);


        return response(['contact' => $contact], 200);
    }
}
