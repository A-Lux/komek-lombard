<?php

namespace App\Http\Controllers\Api;

use App\About;
use App\Http\Controllers\Controller;

class AboutController extends Controller
{
    public function index()
    {
        $abouts = About::get();
        foreach ($abouts as $about){
            $about['image'] = asset('public/storage/'.$about['image']);

        }
        return response(['about' => $abouts]);
    }
}
