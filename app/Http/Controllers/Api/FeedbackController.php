<?php

namespace App\Http\Controllers\Api;

use App\Feedback;
use App\Http\Controllers\Controller;
use App\Mail\FeedbackSent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;

class FeedbackController extends Controller
{
    public function create(Request $request)
    {
        $validator  = Validator::make($request->all(), [
            'username'  => 'required|string|max:255',
            'message'   => 'string',
        ]);

        if($validator->fails()){
            $errors  = $validator->errors();
            return response([
                'message'   => $errors->first()
            ], 200);
        }else{
            $model  = Feedback::create([
                'username'  => $request->username,
                'message'   => $request->message
            ]);

            Mail::to(env('MAIL_FROM_ADDRESS'))->send(new FeedbackSent($model));

            return response([
                'message'   => 'Ваша заявка принята'
            ], 200);
        }
    }
}
