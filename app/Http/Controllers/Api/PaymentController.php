<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Order;

use App\Traits\Paybox;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class PaymentController extends Controller
{
    //

    public function pay(Request $request)
    {
        $valData = $request->validate([
            'amount' => 'required',
            'identifier' => 'required',
            'barcode' => 'required',
        ]);

        $order = Order::create([
            'identifier' => $request->has('identifier') ? $request->identifier : null,
            'amount' => $request->amount,
            'ticket_id' => $request->barcode
        ]);
        if ($request->has('is_seven') && $request->is_seven == 1) {
            $payBox = new Paybox(534025, 1, '6c07S2xERviDoxFr');

        } else {
            $payBox = new Paybox(534022, 1, 'dF6RixgQn0wgY1op');
        }

        $payBox->setQuery([
            'pg_amount' => $request->amount,
            'pg_salt' => Str::random(15),
            'pg_order_id' => $order->id,
            'pg_description' => 'Оплата номер ' . $request->barcode,
            'pg_result_url' => route('result'),
            'pg_success_url' => route('welcome'),
            'pg_failure_url' => route('fail'),
            'pg_request_method' => 'GET'
        ]);

        $query = $payBox->getPaymentLink();


        return response(['payment_url' => $query], 201);


    }

    public function result(Request $request)
    {
        $order = Order::find($request->pg_order_id);
        $order->update([
            'paid' => 1
        ]);

        $monthNow = Carbon::now()->month;
        if ($monthNow < 10){
            $monthNow = '0'.$monthNow;
        }

        $minuteNow = Carbon::now()->minute;
        if ($minuteNow < 10){
            $minuteNow = '0'.$minuteNow;
        }

        $secondNow = Carbon::now()->second;
        if ($secondNow < 10){
            $secondNow = '0'.$secondNow;
        }
        $response = Http::withBasicAuth('web', '112233Qq+')
            ->withHeaders([
                'Content-Type'=>'application/json'
            ])
            ->post('https://komek-l.kz/komek/hs/Mobile/Payment', [
                'date' => (string) Carbon::now()->day.'.'.$monthNow.'.'.Carbon::now()->year.' '.Carbon::now()->hour.':'.$minuteNow.':'.$secondNow,
                'total_payment' => (string)$order->amount,
                'id_payment' => (string)$order->id,
                'id_ticket' => (string)$order->ticket_id
            ]);



        Log::error('Ticket search ' . $response->body(). ' '.Carbon::now()->day.'.'.$monthNow.'.'.Carbon::now()->year.' '.Carbon::now()->hour.':'.$minuteNow.':'.$secondNow);


        return response('OK '.$response->body(). ' '.Carbon::now()->day.'.'.$monthNow.'.'.Carbon::now()->year.' '.Carbon::now()->hour.':'.$minuteNow.':'.$secondNow, 200);
    }
}
