<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Identificator;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    //
    public function store(Request $request)
    {
        $request->validate([
           'identificator' => 'required',
           'device_id' => 'required'
        ]);
        $identificator = Identificator::where('identificator',$request->identificator)->first();
        if ($identificator){
            $identificator->update([
                'device_id' => $request->device_id
            ]);
        }else{
            $identificator = Identificator::create([
                'identificator' => $request->identificator,
                'device_id' => $request->device_id,
            ]);
        }

        return response([],201);
    }



}
