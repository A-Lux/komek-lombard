<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Identificator extends Model
{
    //
    protected $fillable = [
      'identificator',
      'device_id'
    ];
}
