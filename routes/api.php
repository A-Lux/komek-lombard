<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::namespace('Api')->group(function () {
    Route::post('register/identificator','RegisterController@store');
    Route::get('about', 'AboutController@index');
    Route::get('faq', 'FaqController@index');
    Route::get('contacts/{id}', 'ContactsController@index');
    Route::get('pay','PaymentController@pay');
    Route::get('payment/result','PaymentController@result')->name('result');

    Route::post('feedback/create','FeedbackController@create');
});
